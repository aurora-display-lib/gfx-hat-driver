# Pimoroni GFX HAT Driver for Aurora Display Library
This is a driver package for the GFX HAT for the Raspberry Pi made by
Pimoroni. It uses its published `gfxhat` package as a dependency.

> Note: To use this package you will need to provide your own installation
> of Raspberry Pi OS (or compatible) since the Raspberry Pis GPIO bindings
> are required and not available through PIP. Therefore also the `gfxhat`
> library is not listed as a required dependency, however you won't be able
> to run this code on a regular computer anyway.
