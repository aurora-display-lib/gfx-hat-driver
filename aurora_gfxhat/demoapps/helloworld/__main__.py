from PIL import Image
from PIL.ImageDraw import ImageDraw
from aurora.base.app import AuroraApplication
from aurora.base.input import NullInputSource
from aurora.log import configure_logging

from aurora_gfxhat.renderer import GfxHatRenderTarget


class HelloGfxHatApplication(AuroraApplication):
    def __init__(self):
        super().__init__(GfxHatRenderTarget(convert_to_bw=False),
                         NullInputSource(),
                         buffer=Image.new('P', GfxHatRenderTarget.get_screen_size()),
                         limit_render_speed=False)

    def __str__(self) -> str:
        return 'GfxHatBacklight'

    @property
    def gfxhat(self) -> GfxHatRenderTarget:
        # noinspection PyTypeChecker
        return self._render_target

    def _setup(self):
        super()._setup()
        self.gfxhat.backlight.set_all_colors((150, 150, 150))

    def _render(self, draw: ImageDraw, t: float):
        draw.text((10, 10), 'Hello GFX HAT!', fill=1)
        draw.text((10, 30), 't={:.3f}'.format(t), fill=1)


if __name__ == '__main__':
    configure_logging()
    HelloGfxHatApplication().run()
