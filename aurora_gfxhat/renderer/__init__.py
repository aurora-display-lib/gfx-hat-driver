from PIL.Image import Image
from aurora.base.render import AuroraRenderTarget
from aurora.types import TwoDimSize, RendererExtras

from time import time

from aurora_gfxhat.renderer.extras import EXTRA_BACKLIGHT
from aurora_gfxhat.renderer.extras.backlight import GfxHatBacklightExtra

from gfxhat import lcd


class GfxHatRenderTarget(AuroraRenderTarget):
    """
    GFX HAT rendering implementation
    """
    def __init__(self,
                 convert_to_bw: bool = True,
                 render_background_on_every_frame: bool = True):
        """
        Initializes a new render target for a GFX HAT device
        :param convert_to_bw: If enabled, will always convert the provided image to a b/w image first
        :param render_background_on_every_frame: If enabled, will set the background color on every frame
        """
        super().__init__()

        self._convert_to_bw: bool = convert_to_bw
        self._render_background_on_every_frame: bool = render_background_on_every_frame
        self._extras: RendererExtras = {
            EXTRA_BACKLIGHT: GfxHatBacklightExtra()
        }

    def init(self):
        super().init()
        lcd.clear()
        self.backlight.init()

    @staticmethod
    def get_screen_size() -> TwoDimSize:
        return lcd.dimensions()

    @property
    def screen_size(self) -> TwoDimSize:
        return lcd.dimensions()

    @property
    def backlight(self) -> GfxHatBacklightExtra:
        return self._extras[EXTRA_BACKLIGHT]

    def render_image(self, image: Image, render_time: float):
        t_start = time()
        self._logger.debug('Rendering to gfxhat ...')

        img_buf = image.copy()
        if self._convert_to_bw:
            img_buf = img_buf.convert('P')

        width, height = self.screen_size
        for x in range(width):
            for y in range(height):
                p = img_buf.getpixel((x, y))
                lcd.set_pixel(x, y, p)

        lcd.show()

        t_end = time() - t_start
        self._logger.debug('Finished rendering, took %.4f', t_end)

        if self._render_background_on_every_frame and self.backlight.has_changes:
            self._logger.debug('Will render backlight changes, since changes are available')
            self.backlight.render()

    def __str__(self) -> str:
        return 'render.GFXHAT'
