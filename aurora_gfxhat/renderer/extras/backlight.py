from typing import List

from aurora.base.base import LoggingObject
from aurora.types import RgbColor

from gfxhat import backlight, lcd


class InternalGfxHatBacklightChange(object):
    def __init__(self,
                 color: RgbColor,
                 index: int = 0,
                 all_colors: bool = True):
        self._index = index
        self._color = color
        self._all_color = all_colors

    def apply(self):
        r, g, b = self._color
        if self._all_color:
            backlight.set_all(r, g, b)
        else:
            backlight.set_pixel(self._index, r, g, b)


class GfxHatBacklightExtra(LoggingObject):
    def __init__(self):
        super().__init__('extra.GfxHatBacklight')
        self._queue: List[InternalGfxHatBacklightChange] = []

    def init(self):
        self._logger.debug('Initializing Backlight ...')
        backlight.setup()
        lcd.contrast(45)

    def set_color(self,
                  index: int,
                  color: RgbColor):
        self._logger.debug('Setting backlight color %.0f to %s', index, color)
        self._queue.append(InternalGfxHatBacklightChange(color, index=index))

    def set_all_colors(self,
                       color: RgbColor):
        self._logger.debug('Setting all backlights to %s', color)
        self._queue.append(InternalGfxHatBacklightChange(color, all_colors=True))

    @property
    def has_changes(self) -> bool:
        return len(self._queue) > 0

    def render(self):
        self._logger.debug('Rendering %.0f backlight changes', len(self._queue))
        for change in self._queue:
            change.apply()
        backlight.show()

        self._logger.debug('Clearing backlight change queue')
        self._queue.clear()
